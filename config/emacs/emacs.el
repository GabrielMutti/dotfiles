(defun reload-emacs-config ()
  (interactive)
  (message "Loading init file...")
  (load-file "~/.emacs.d/init.el"))

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(setq package-list '(helm vterm multi-vterm web-mode nyan-mode ace-jump-mode projectile org cmake-mode helm-projectile lsp-mode lsp-ui helm-lsp auto-rename-tag prettier))

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

(require 'use-package)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-vibrant t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;; Disables the menu
(menu-bar-mode -1)

;; Disables the scroll bar
(menu-bar-no-scroll-bar)

;; Disables the tool bar
(tool-bar-mode -1)

;; Degenerate mode=on
(nyan-mode 1)

;; Show line and column number always
(setq column-number-mode t)
(setq line-number-mode t)
(global-display-line-numbers-mode)

;; Sets default tab widt
(setq-default tab-width 4)

;; Spaces as tabs
(setq-default indent-tabs-mode nil)

(setq org-startup-indented t)

;; Setups helm keys
(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-set-key (kbd "C-x b") #'helm-buffers-list)

;; Auto-enable web-mode for common files
(add-to-list 'auto-mode-alist '("\\.jsx?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ts?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tsx?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.json?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css?$" . web-mode))

;; Auto-enable react for jsx files
(setq web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'")))

;; Web mode hook
(defun my-web-mode-hook ()
  (flycheck-mode)
  (company-mode)
  (lsp)
  (lsp-ui-mode)
  (auto-rename-tag-mode t)
  (prettier-mode))
(add-hook 'web-mode-hook  'my-web-mode-hook)

(setq web-mode-enable-current-element-highlight t)
(setq web-mode-enable-current-column-highlight t)
(setq flycheck-check-syntax-automatically '(mode-enabled save))



;; LSP mode configuration
(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")

  :hook ((web-mode . lsp))

  :commands lsp)

(use-package helm-lsp :commands helm-lsp-workspace-symbol)

(require 'ace-jump-mode)
(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)

(projectile-mode +1)
;; Recommended keymap prefix on Windows/Linux
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(setq vterm-timer-delay nil)
(add-hook 'vterm-mode-thook
          (define-key global-map (kbd "C-c v t") 'multi-vterm)
          (define-key global-map (kbd "C-c v n") 'multi-vterm-next)
          (define-key global-map (kbd "C-c v p") 'multi-vterm-prev))

(use-package lsp-ui
  :commands lsp-ui-mode
  :config (setq lsp-ui-doc-delay 1))

;; Fixing eshell scroll behavior
(add-hook 'eshell-mode-hook
          (defun chunyang-eshell-mode-setup ()
            (remove-hook 'eshell-output-filter-functions
                         'eshell-postoutput-scroll-to-bottom)))

(defun on-c++-mode-exit-hook ()
  (remove-hook 'after-save-hook 'clang-format-buffer))

(defun my-c++-mode-hook ()
  (company-mode)
  (flycheck-mode)
  (lsp)
  (lsp-ui-mode)
  (yas-minor-mode)
  (setq c-basic-offset 4)
  (setq c-set-style "k&r")
  (add-hook 'after-save-hook 'clang-format-buffer)
  (add-hook 'change-major-mode-hook 'on-c++-mode-exit-hook))

(add-hook 'c++-mode-hook 'my-c++-mode-hook)

(defun on-c-mode-exit-hook ()
  (remove-hook 'after-save-hook 'clang-format-buffer))

(defun my-c-mode-hook ()
  (company-mode)
  (flycheck-mode)
  (lsp)
  (lsp-ui-mode)
  (yas-minor-mode)
  (setq c-basic-offset 4)
  (setq c-set-style "k&r")
  (add-hook 'after-save-hook 'clang-format-buffer)
  (add-hook 'change-major-mode-hook 'on-c-mode-exit-hook))

(add-hook 'c-mode-hook 'my-c-mode-hook)

;; Backup files location
(setq backup-directory-alist `(("." . "~/.emacs.d/backup")))

(setq initial-buffer-choice "~/.config/emacs/start.org")
