#+TITLE: Fish Configuration
#+AUTHOR: Gabriel Teixeira
#+PROPERTY: header-args :tangle "~/.config/fish/config.fish"

* Fish Config

** Startup

Executes "sway":

#+BEGIN_SRC sh
  if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
    exec sway

    exec emacs --daemon
    return;
  end
#+END_SRC

Sets code path for interative commands:

#+BEGIN_SRC sh
  if status is-interactive
     # Commands to run in interactive sessions can go here
  end
#+END_SRC


** Path

Adds "cargo" to the PATH variable:

#+BEGIN_SRC sh
fish_add_path ~/.cargo/bin
#+END_SRC

** Alias

Configures the "gits" alias, for "git status":

#+BEGIN_SRC sh
alias gits="git status"
#+END_SRC

** Vterm integration

Defines a function to enable integration between Emacs' vterm and fish:

#+BEGIN_SRC sh
function vterm_printf;
    if begin; [  -n "$TMUX" ]  ; and  string match -q -r "screen|tmux" "$TERM"; end 
        # tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$argv"
    else if string match -q -- "screen*" "$TERM"
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$argv"
    else
        printf "\e]%s\e\\" "$argv"
    end
end
#+END_SRC

Adds directory tracking:

#+BEGIN_SRC sh
function vterm_prompt_end;
    vterm_printf '51;A'(whoami)'@'(hostname)':'(pwd)
end
functions --copy fish_prompt vterm_old_fish_prompt
function fish_prompt --description 'Write out the prompt; do not replace this. Instead, put this at end of your file.'
    # Remove the trailing newline from the original prompt. This is done
    # using the string builtin from fish, but to make sure any escape codes
    # are correctly interpreted, use %b for printf.
    printf "%b" (string join "\n" (vterm_old_fish_prompt))
    vterm_prompt_end
end
#+END_SRC


