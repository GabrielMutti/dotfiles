#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\[\e[0;38;5;254m\][\[\e[0;38;5;40m\]\u\[\e[0;38;5;40m\]@\[\e[0;38;5;40m\]\h \[\e[0;38;5;224m\]\w\[\e[0;38;5;254m\]]\[\e[0;97m\]\$ \[\e[0m\]'

export ALTERNATE_EDITOR=""
export EDITOR="emacseclient -t"
export VISUAL="emacsclient -c -a emacs"
export MOZ_ENABLE_WAYLAND=1

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

function ec() {
    emacsclient -c "$@" &
}
export NVS_HOME="$HOME/.nvs"
[ -s "$NVS_HOME/nvs.sh" ] && . "$NVS_HOME/nvs.sh"
