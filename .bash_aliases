alias rm="rm -i"
alias mv="mv -i"
alias gits="git status"
alias la="ls -al"
alias kill-emacs="emacsclient --eval \"(kill-emacs)\""
